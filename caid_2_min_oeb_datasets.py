#!/usr/bin/env python

import argparse
import datetime
import json
import os
import os.path

def caid_transcoder(input_filenames, output_directory, community_acronym="CAID", benchmarking_event_label="CAID1"):
	challenges = dict()
	print(f"* Reading {len(input_filenames)} files")
	for input_filename in input_filenames:
		print(f"\t- Parsing {input_filename}")
		iF_stat = os.stat(input_filename)
		with open(input_filename, mode="r", encoding="utf-8") as iF:
			o_agg = json.load(iF)
			challenge_label = o_agg["challenge_ids"][0]
			challenges.setdefault(challenge_label, []).append((o_agg, iF_stat))
	
	print(f"* Saving to {output_directory}")
	os.makedirs(output_directory, exist_ok=True)
	for challenge_label, o_aggs in challenges.items():
		part_d = dict()
		ass_d = []
		agg_d = []
		# Gathering the participants
		# and their metrics
		unique_participants = set()
		for o_agg, iF_stat in o_aggs:
			metric_label = o_agg["datalink"]["inline_data"]["visualization"]["x_axis"]
			cha_pa = []
			for m in o_agg["datalink"]["inline_data"]["challenge_participants"]:
				participant_label = m["participant_id"]
				metric_value = m["metric_x"]
				
				# Creating the participant dataset
				p = part_d.get(participant_label)
				if p is None:
					mod_date = datetime.datetime.fromtimestamp(iF_stat.st_mtime, tz=datetime.timezone.utc).astimezone().isoformat()
					p = {
						"_id": f"{community_acronym}:{benchmarking_event_label}_{challenge_label}_{participant_label}_P",
						"challenge_id": [
							challenge_label
						],
						"community_id": community_acronym,
						"datalink": {
							"attrs": [
								"archive"
							],
							"status": "ok",
							"validation_date": mod_date,
						},
						"participant_id": participant_label,
						"type": "participant"
					}
					part_d[participant_label] = p
				
				# Now the assessment dataset
				a_d = {
					"_id": f"{community_acronym}:{benchmarking_event_label}_{challenge_label}_{metric_label}_{participant_label}_A",
					"metrics": {
						"metric_id": metric_label,
						"value": metric_value,
					},
					"participant_id": participant_label,
					"challenge_id": challenge_label,
					"community_id": community_acronym,
					"type": "assessment",
				}
				ass_d.append(a_d)
				c_p = {
					"participant_id": participant_label,
					"metric_value": metric_value,
				}
				cha_pa.append(c_p)
			
			# And the aggregation dataset
			g_d = {
			    "_id": f"{community_acronym}:{benchmarking_event_label}_{challenge_label}_agg",
			    "community_id": community_acronym,
			    "challenge_ids": [
				challenge_label,
			    ],
			    "datalink": {
				"inline_data": {
					"challenge_participants": cha_pa,
					"visualization": {
						"type": "bar-plot",
						"metric": metric_label,
					}
				}
			    },
			    "type": "aggregation"
			}
			agg_d.append(g_d)
		
		# All together
		challenge_l = []
		challenge_l.extend(part_d.values())
		challenge_l.extend(ass_d)
		
		# Declaring the participants for this challenge
		cha_d = {
			"id": challenge_label,
			"participants": list(part_d.keys())
		}
		challenge_l.append(cha_d)
		
		challenge_l.extend(agg_d)
		
		print(f"\t- Writing challenge {challenge_label}")
		with open(os.path.join(output_directory, challenge_label + ".json"), mode="w", encoding="utf-8") as oC:
			json.dump(challenge_l, oC, indent=4, sort_keys=True)
		

def main():
	ap = argparse.ArgumentParser(
		description="Prototype CAID -> OEB minimal datasets transcoder",
		formatter_class=argparse.ArgumentDefaultsHelpFormatter,
	)
	
	ap.add_argument(
		"-i",
		dest="input_filenames",
		nargs="+",
		required=True,
		help="Input JSON filenames from CAID"
	)
	
	ap.add_argument(
		"-o",
		required=True,
		dest="output_directory",
		help="Output directory whose contents follow the minimal dataset schemas"
	)
	
	args = ap.parse_args()
	
	caid_transcoder(args.input_filenames, args.output_directory)
	
	

if __name__ == "__main__":
	main()