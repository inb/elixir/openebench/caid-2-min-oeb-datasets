# CAID 2 Min OEB Datasets

## Usage

```bash
# Transcoding the dataset in the minimal schema
python caid_2_min_oeb_datasets.py -i generated_benchmark_data/*.json -o min_datasets
```

## Validation of generated content (living in `min_datasets`)

```bash
# In order to get the schema and tools to use for the validation
git clone https://github.com/inab/OEB_level2_data_migration.git

python3 -mvenv .py3env
source .py3env/bin/activate
pip install --upgrade pip wheel
pip install extended-json-schema-validator

# Now, the validation as such of each generated file
for A in min_datasets/*json ; do
    ext-json-validate --guess-schema --report "$(dirname "$A")/val_report_$(basename "$A")" --iter-arrays OEB_level2_data_migration/oeb_level2/schemas/minimal_bdm_oeb_level2.yaml "$A"
done
```
